#ifndef EV_ROBOT_HARDWARE_H
#define EV_ROBOT_HARDWARE_H

#define DUAL_LIDARS

namespace EVSensors
{
	class IMU;
	class Lidar;
	class LimitSwitch;
	class Sonar;
}

extern EVSensors::IMU* Imu;
extern EVSensors::Lidar* LiDAR;
extern EVSensors::Lidar* LiDAR2;
extern EVSensors::Sonar* USonic;

extern EVSensors::LimitSwitch* LimSwitch_Bottom;
extern EVSensors::LimitSwitch* LimSwitch_Top;
extern EVSensors::LimitSwitch* LimSwitch_Left;
extern EVSensors::LimitSwitch* LimSwitch_Right;

#endif // !EV_ROBOT_HARDWARE_H
