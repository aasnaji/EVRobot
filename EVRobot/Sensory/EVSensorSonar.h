#ifndef EV_SENSOR_SONAR_H
#define EV_SENSOR_SONAR_H

#include "inttypes.h"
namespace EVSensors
{
	class Sonar;
}

class EVSensors::Sonar
{
	struct FSonarHeader
	{
		uint8_t TriggerPin : 4;
		uint8_t EchoPin : 4;
		uint8_t EchoTimeoutDistance : 8;		// The maximum distance in cm the ultrasonic can detect
	};

public:
	Sonar(uint8_t triggerPin, uint8_t echoPin);
	~Sonar();

	float RequestMeasurement();

	void SetMaxRange(uint8_t distance_cm);
	uint8_t GetMaxRange() const;

private:

	FSonarHeader m_sonarParams;
};
#endif // !EV_SENSOR_SONAR_H
