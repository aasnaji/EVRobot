#ifndef EV_SENSOR_IMU_H
#define EV_SENSOR_IMU_H

#include "Utils\EVMath\EVMVectors.h"

class MPU9250;
namespace EVSensors
{
	class IMU;
}

class EVSensors::IMU
{
	struct FIMUData
	{
		FVector Accel;
		FVector Gyro;
	};

public:
	IMU();
	~IMU();

	void Init();

	FVector AccelVector();
	FVector GyroVector();

private:
	MPU9250* m_imu;
};
#endif // !EV_SENSOR_IMU_H
