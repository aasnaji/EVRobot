#ifndef EV_SENSOR_LIMIT_SWITCH_H
#define EV_SENSOR_LIMIT_SWITCH_H
#include "inttypes.h"

namespace EVSensors
{
	class LimitSwitch;
}

class EVSensors::LimitSwitch
{
	struct FSwitchHeader
	{
		uint8_t PinNumber : 6;
		uint8_t NormallyClosed : 1;
		uint8_t LastVal : 1;
	};

public:
	/* Callback to be invoked when the switch state changes */
	typedef void(*SwitchTriggeredCallback)(LimitSwitch*, bool);
	SwitchTriggeredCallback SwitchTriggerCb;

	LimitSwitch(uint8_t pinNumber, bool normallyClosed = 1);
	~LimitSwitch();

	bool IsTriggered();
	void OnTrigger();
private:
	FSwitchHeader m_switchParams;
};
#endif // !EV_SENSOR_LIMIT_H
