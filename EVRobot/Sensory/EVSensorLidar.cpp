#include "EVSensorLidar.h"

#include "Libraries\AdafruitVL53L0\Adafruit_VL53L0X.h"

#include "Utils\EVLogger.h"
#include "Utils\EVMath\EVMFunctions.h"

using namespace EVSensors;

#define MAX_DETECT_DISTANCE					2000 // mm

Lidar::Lidar(int i2cAddress, int shutdownPin)
{
	m_lidar = nullptr;
	m_i2cAddress = i2cAddress;
	m_shutdownPin = shutdownPin;
	m_clampedDistance = 8192;
}

Lidar::~Lidar()
{
	delete m_lidar;
}

void Lidar::Init()
{
	m_lidar = new Adafruit_VL53L0X();

	if (!m_lidar->begin(m_i2cAddress))
	{
		LOG("Single LiDAR Init Failed..."); LOG("\n");
		delay(5);
		while (1)
		{
		}
	}

	LOG("LiDAR Initialized"); LOG("\n");
}

void Lidar::SetRange(ELidarRange range)
{
	m_lidar->setRange(static_cast<uint8_t>(range));
}

void Lidar::SetClampedDistance(uint16_t clampedDist)
{
	m_clampedDistance = clampedDist;
}

// Returns distance measured in cm
float Lidar::Distance()
{
	return Distance_mm()/10.0;
}

float Lidar::Distance_mm()
{
	VL53L0X_RangingMeasurementData_t measurement;

	m_lidar->getSingleRangingMeasurement(&measurement);

	// Phase Failure, data incorrect or clipped.
	if (measurement.RangeStatus == 4)
	{
		return m_clampedDistance;
	}

	return FMath::Clamp(0, m_clampedDistance, measurement.RangeMilliMeter);
}

int Lidar::GetAddress() const
{
	return m_i2cAddress;
}

uint16_t EVSensors::Lidar::GetClampedDistance() const
{
	return m_clampedDistance;
}



void FLidarInitializer::InitDualLidars(Lidar* lidar1, Lidar* lidar2)
{
	if (!lidar1 || !lidar2)
	{
		LOG("Dual LiDAR initialization failed: Invalid Lidar objects..."); LOG("\n");
		delay(50);
		while (1) {}
	}

	lidar1->m_lidar = new Adafruit_VL53L0X();
	lidar2->m_lidar = new Adafruit_VL53L0X();

	int SHT_LOX1 = lidar1->m_shutdownPin;
	int SHT_LOX2 = lidar2->m_shutdownPin;
	
	// Initialize the GPIOs for the shutdown pins
	pinMode(SHT_LOX1, OUTPUT);
	pinMode(SHT_LOX2, OUTPUT);
	
	// all reset
	digitalWrite(SHT_LOX1, LOW);
	digitalWrite(SHT_LOX2, LOW);
	delay(10);
	// all unreset
	digitalWrite(SHT_LOX1, HIGH);
	digitalWrite(SHT_LOX2, HIGH);
	delay(10);

	// activating LOX1 and reseting LOX2
	digitalWrite(SHT_LOX1, HIGH);
	digitalWrite(SHT_LOX2, LOW);

	// initing LOX1
	if (!lidar1->m_lidar->begin(lidar1->m_i2cAddress)) {
		Serial.println(F("Failed to boot first VL53L0X"));
		delay(50);
		while (1);
	}
	delay(10);

	// activating LOX2
	digitalWrite(SHT_LOX2, HIGH);
	delay(10);

	//initing LOX2
	if (!lidar2->m_lidar->begin(lidar2->m_i2cAddress)) {
		Serial.println(F("Failed to boot second VL53L0X"));
		delay(50);
		while (1);
	}

	LOG("LiDAR Dual Init Complete"); LOG("\n");
}

void FLidarInitializer::InitSingleLidar(Lidar* lidar)
{
	if (lidar)
	{
		lidar->Init();
	}
}
