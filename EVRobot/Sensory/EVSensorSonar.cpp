#include "EVSensorSonar.h"
#include "Arduino.h"

#define TRIGGER_HOLD_TIME					0.01										// 0.01 ms indicates 10 micros needed for trigger to generated pulses	
#define ECHO_TIMEOUT_MICRO					(m_sonarParams.EchoTimeoutDistance/0.034)	// yields a timeout such that a distance greater than timeout distance is invalidated
#define ECHO_TO_DIST(PULSEWIDTH)			(PULSEWIDTH * (0.034/2))					// Converts an echo pulse reading to distance measurement in cm

#define MAX_DETECT_DISTANCE					2000

EVSensors::Sonar::Sonar(uint8_t triggerPin, uint8_t echoPin)
{
	pinMode(triggerPin, OUTPUT);
	pinMode(echoPin, INPUT);
	m_sonarParams.TriggerPin = triggerPin;
	m_sonarParams.EchoPin = echoPin;
}

EVSensors::Sonar::~Sonar()
{
}

float EVSensors::Sonar::RequestMeasurement()
{
	// Reset Trigger
	digitalWrite(m_sonarParams.TriggerPin, LOW);
	delayMicroseconds(2);

	// Apply 10 us trigger
	digitalWrite(m_sonarParams.TriggerPin, HIGH);
	delayMicroseconds(TRIGGER_HOLD_TIME);
	digitalWrite(m_sonarParams.TriggerPin, LOW);

	auto duration = pulseIn(m_sonarParams.EchoPin, HIGH, ECHO_TIMEOUT_MICRO);
	float distance = ECHO_TO_DIST(duration);

	return distance;
}

void EVSensors::Sonar::SetMaxRange(uint8_t distance_cm)
{
	m_sonarParams.EchoTimeoutDistance = distance_cm;
}

uint8_t EVSensors::Sonar::GetMaxRange() const
{
	return m_sonarParams.EchoTimeoutDistance;
}
