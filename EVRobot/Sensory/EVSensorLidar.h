#ifndef EV_SENSOR_LIDAR_H
#define EV_SENSOR_LIDAR_H

#include "inttypes.h"

class Adafruit_VL53L0X;
namespace EVSensors
{
	class Lidar;
	class FLidarInitializer;
}

class EVSensors::FLidarInitializer
{
public:
	void InitDualLidars(EVSensors::Lidar*, EVSensors::Lidar*);
	void InitSingleLidar(EVSensors::Lidar*);
};

class EVSensors::Lidar
{
	friend class FLidarInitializer;
public:
	enum class ELidarRange
	{
		Default = 0,
		HighAccuracy = 1,
		LongRange = 2,
		HighSpeed = 3
	};

	Lidar(int i2cAddress, int shutdownPin);
	~Lidar();

	void SetRange(ELidarRange);
	void SetClampedDistance(uint16_t);
	
	float Distance();
	float Distance_mm();

	int GetAddress() const;
	uint16_t GetClampedDistance() const;
private:
	void Init();

	Adafruit_VL53L0X* m_lidar;
	uint16_t m_clampedDistance;
	int m_i2cAddress;
	int m_shutdownPin;
};
#endif // !EV_SENSOR_LIDAR_H

