#include "EVSensorLimitSwitch.h"

#include "Arduino.h"
using namespace EVSensors;

LimitSwitch::LimitSwitch(uint8_t pinNumber, bool normallyClosed = 1)
	: SwitchTriggerCb(nullptr)
{
	m_switchParams.PinNumber = pinNumber;
	m_switchParams.NormallyClosed = normallyClosed;
	m_switchParams.LastVal = normallyClosed;

	pinMode(pinNumber, INPUT_PULLUP);
}

LimitSwitch::~LimitSwitch()
{
}

bool LimitSwitch::IsTriggered()
{
	uint8_t readVal = digitalRead(m_switchParams.PinNumber);

	
	bool trigger =  m_switchParams.NormallyClosed ? readVal == 0 && m_switchParams.LastVal != 0
												  : readVal == 1 && m_switchParams.LastVal != 1;
												  
	m_switchParams.LastVal = readVal;
	return trigger;
}

void EVSensors::LimitSwitch::OnTrigger()
{
	if (SwitchTriggerCb)
		SwitchTriggerCb(this, m_switchParams.LastVal);
}

