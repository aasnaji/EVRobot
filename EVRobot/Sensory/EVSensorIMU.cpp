#include "EVSensorIMU.h"

#include "Libraries\IMU\src\MPU9250.h"
#include "Wire.h"

#include "Utils\EVLogger.h"
using namespace EVSensors;

IMU::IMU()
	: m_imu(nullptr)
{
}

IMU::~IMU()
{
	delete m_imu;
}

void IMU::Init()
{
	//LOG("IMU Sensor Init"); LOG("\n");
	delay(5);

	if (m_imu)
	{
		LOG("IMU Already Initialized"); LOG("\n");
	}

	m_imu = new MPU9250(Wire, 0x68);

	if (m_imu->begin() != 1)
	{
		LOG("IMU Init Failed"); LOG("\n");
		delay(100);
		while (1) {}
	}

	m_imu->setGyroRange(MPU9250::GYRO_RANGE_250DPS);
	m_imu->setAccelRange(MPU9250::ACCEL_RANGE_2G);
	LOG("IMU Init Completed"); LOG("\n");
}

FVector IMU::AccelVector()
{
	if (!m_imu->readSensor())
	{
		LOG("FAILED TO READ IMU"); LOG("\n");
	}

	// Notice the coordinate convention:
	// X - Up Vector
	// Y - Right Vector
	// Z - Forward Vector
	return FVector(	m_imu->getAccelX_mss()
				  , m_imu->getAccelY_mss()
				  , m_imu->getAccelZ_mss());
}

FVector IMU::GyroVector()
{
	if (!m_imu->readSensor())
	{
		LOG("FAILED TO READ IMU"); LOG("\n");
	}
	
	// Notice the coordinate convention:
	// X - Up Vector
	// Y - Right Vector
	// Z - Forward Vector
	return FVector(m_imu->getGyroX_rads()
				 , m_imu->getGyroY_rads()
				 , m_imu->getGyroZ_rads());
}
