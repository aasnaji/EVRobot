/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EV_DRIVE_H
#define EV_DRIVE_H

#include "inttypes.h"

enum class EServoSpeed
{
	Idle,
	Low,
	Medium,
	High
};

namespace EVDrive
{
	void InitDrive();

	void MoveForward(EServoSpeed);
	void MoveBackward(EServoSpeed);

	void TurnRight(float DeltaDegrees);
	void TurnLeft(float DeltaDegrees);

	void RaiseArms();
	void LowerArms();
	void ArrestArms();
	void ResetArms();

	void Stop();
	void Release();

	/*  Reverse the direction convention of the drive system to indicate
	*	a change or reflection of the driven body's coordinates. This ensures
	*	that application code remains consistent with its expressiveness of 
	*	movement directions.
	*/
	void ReverseDriveConvention();
	void ResetDriveConvention();

	/*	Enables Deviation Correction based on a set distance from
	*	an observable boundary. If the boundary is no longer read,
	*	applies no correction. This drive uses the secondary ToF sensor
	*	to detect and hug boundaries.
	*/
	typedef float(*MeasureDistFunction)();
	void ApplyDeviationCorrection(float SetPoint,MeasureDistFunction MeasureFunc );
	void ApplyDeviationCorrection(float SetPoint, float Measured);
}
#endif // !EV_DRIVE_H
