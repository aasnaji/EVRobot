#include "EVDrive.h"
#include "Private/EVServosImpl.h"

#include "Utils\EVLogger.h"
#include "Utils\EVMath\EVMFunctions.h"

typedef void(*ServoMoveFunc)(EServoSpeed);
static ServoMoveFunc moveFwd;
static ServoMoveFunc moveBack;

typedef void(*ServoTurnFunc)(float);
static ServoTurnFunc turnRight;
static ServoTurnFunc turnLeft;

typedef void(*ServoStepFunc)(int);
static ServoStepFunc stepRight;
static ServoStepFunc stepLeft;

static float prevDeviationError = 0;
static unsigned long lastCorrectionTime = 0;

void EVDrive::InitDrive()
{
	LOG("Initialized Drive"); LOG("\n");
	ResetDriveConvention();
	EVServos::InitServos();
}

void EVDrive::MoveForward(EServoSpeed speed)
{
	moveFwd(speed);
}

void EVDrive::MoveBackward(EServoSpeed speed)
{
	moveBack(speed);
}

void EVDrive::TurnRight(float DeltaDegrees)
{
	turnRight(DeltaDegrees);
}

void EVDrive::TurnLeft(float DeltaDegrees)
{
	turnLeft(DeltaDegrees);
}

void EVDrive::RaiseArms()
{
	EVServos::RaiseArms_Impl();
}

void EVDrive::LowerArms()
{
	EVServos::LowerArms_Impl();
}

void EVDrive::ArrestArms()
{
	EVServos::ArrestArms_Impl();
}

void EVDrive::ResetArms()
{
	EVServos::ResetArms_Impl();
}

void EVDrive::Stop()
{
	EVServos::Stop_Impl();
}

void EVDrive::Release()
{
	EVServos::Stop_Impl();
}

void EVDrive::ReverseDriveConvention()
{
	moveFwd = EVServos::MoveBackward_Impl;
	moveBack = EVServos::MoveForward_Impl;

	turnRight = EVServos::TurnLeft_Impl;
	turnLeft = EVServos::TurnRight_Impl;

	stepRight = EVServos::StepRight_Impl;
	stepLeft = EVServos::StepLeft_Impl;

	prevDeviationError = 0;
}

void EVDrive::ResetDriveConvention()
{
	moveFwd = EVServos::MoveForward_Impl;
	moveBack = EVServos::MoveBackward_Impl;

	turnRight = EVServos::TurnRight_Impl;
	turnLeft = EVServos::TurnLeft_Impl;

	stepRight = EVServos::StepLeft_Impl;
	stepLeft = EVServos::StepRight_Impl;

	prevDeviationError = 0;
}

void EVDrive::ApplyDeviationCorrection(float SetPoint, MeasureDistFunction MeasureFunc)
{
	if (!MeasureFunc)
		return;

	float Distance = MeasureFunc();
	float Kp = 1.00;
	float Delta = Kp * (SetPoint - Distance);

	// Noting that set points are relative to the right hand side of the forward direction:
	// Implies "Left of target distance"
	if (Distance > SetPoint)
	{
		stepRight(Delta);
	}
	// Implies "Right of target distance"
	else if(Distance < SetPoint)
	{
		stepLeft(Delta);
	}
}

void EVDrive::ApplyDeviationCorrection(float SetPoint, float Measured)
{
#if 0
	if (!stepRight || !stepLeft)
		return;

	float Kp = 1;
	float Kd = 1;
	float Ki = 1;

	// We assume a previous correction process ended if this time-out is reached
	// Simply reset our parameters.
	if (lastCorrectionTime > 5000)
	{
		prevDeviationError = 0;
		lastCorrectionTime = millis();
		delay(100);
	}

	// In this case, the error is a distance in cm
	float error = SetPoint - Measured;

	/* Apply PID processing on error */
	float dt = (millis() - lastCorrectionTime) / 1000.0;

	// Proportional Gain
	float u = Kp*error
			+ Kd* FMath::Differentiate(prevDeviationError, error, dt)
			+ Ki* FMath::Cumtrapz(prevDeviationError, error, dt);

	int PWM = u * 1; //x
	stepLeft(PWM);

	prevDeviationError = error;
	lastCorrectionTime = millis();
#endif
}

