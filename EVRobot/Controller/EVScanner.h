#ifndef EV_SCANNER_H
#define EV_SCANNER_H

#include "Utils\EVContainers\EVContainersFwd.h"

namespace EVScanner
{
	enum class EFacingDirection
	{
		Left,
		Center,
		Right
	};

	struct FScanHit
	{
		unsigned long TimeStamp; // ms
		float LidarDist;
		float SonarDist;
		int RelativeAngle;
	};

	// Called at the start of the robot's initialization. Samples multitude of
	// samples from the ultrasonic and lidar and determines the average value
	// that identifies the background/boundaries.
	void Calibrate(int samples);

	/*	Returns the average distances the lidar and sonar
	*	detected as part of the calibration.
	*/
	float GetLidarAverage();
	float GetSonarAverage();

	/*	Change the comparison equality operation. The scanner
	*	will use the newly assigned equality function to determine
	*	if the reading represents a desired target.
	*
	*	Notice: You can swap this function out to distinguish between
	*	ramp and target detection via the same scan method!
	*
	*	@Param float: Lidar Distance cm
	*	@Param float: Sonar Distance cm
	*/
	typedef bool(*ComparisonFunc)(float, float);

	/*	Scan
	*
	*	Issues a distance measurement from both Sonar & Lidar. The results
	*	of their distance measurements are output as a scan hit result.
	*	If a comparison function is provided, the scan will return true or
	*	false based on the result of the comparison. Otherwise, always returns
	*	false.
	*
	*	@Param FScanHit& outHitInfo: The scan information that will be populated
	*	@Param ComparisonFunc comparisonFunc: The function that will be used for comparison of hit result
	*/
	bool Scan(FScanHit& outHitInfo, ComparisonFunc comparisonFunc = nullptr);

	/*	Sweep
	*
	*	Performs a scan that spans a 180 degrees cone and outputs the resulting
	*	hit information to a list. If a comparison function is provided, the sweep
	*	stops upon a successful comparison and returns the intermediate list, with the
	*	last element being the successful hit.
	*/
	bool Sweep(EV::TList<FScanHit>& outHitsInfo, int scanSamples = 10, ComparisonFunc comparisonFunc = nullptr);

	/*	Set Direction
	*
	*	Sets the facing direction of the scanner by orienting the associated servo
	*/
	void SetDirection(EFacingDirection dir);

	/*	Initializes default variables and states. This will reset
	*	existing calibration information and other set modifications
	*/
	void InitDefaults();
}
#endif // !EV_SCANNER_H
