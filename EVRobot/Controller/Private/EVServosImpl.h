#ifndef EV_SERVOS_IMPL_H
#define EV_SERVOS_IMPL_H

#include "Controller\EVDrive.h"
#include "Servo.h"

namespace EVServos
{
	struct FServoHandle
	{
		public:
		FServoHandle(uint8_t PinNumber);

		void Init();

		/*	Adds additional convenience by automatically re-attaching
		*	the servo if we ever release it.
		*/
		void Write(int val);
		
		/*	Relinquishes holding torque on the servo by detaching it
		*/
		void Release();

		/* Resets servo position by writing the midway 90 degrees value */
		void Reset();

		int Value() const;

		uint8_t PinNumber() const;

		private:
		Servo m_servo;
		uint8_t m_pin;
		int m_val;
	};


	/* Wheel Servos Implementations */
	void MoveForward_Impl(EServoSpeed);
	void MoveBackward_Impl(EServoSpeed);

	void TurnLeft_Impl(float DeltaDegree);
	void TurnRight_Impl(float DeltaDegrees);

	/* Adds incremental PWM steps to either directions */
	void StepLeft_Impl(int deltaPWM);
	void StepRight_Impl(int deltaPWM);

	void Stop_Impl();
	void Release_Impl();

	/* Arm Servos Implementations */
	void RaiseArms_Impl();
	void LowerArms_Impl();
	void ArrestArms_Impl();
	void ResetArms_Impl();

	void InitServos();

	FServoHandle* GetServo(uint8_t InPin);

	/* DEBUGGING */
	FServoHandle& _DEBUG_GetLeftWheel();
	FServoHandle& _DEBUG_GetRightWheel();
}
#endif // !EV_SERVOS_IMPL_H
