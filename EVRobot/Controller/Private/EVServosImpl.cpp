#include "EVServosImpl.h"

#include "EVPinMap.h"
#include "Servo.h"
#include "inttypes.h"

// Use the Imu to control rotation
#include "EVRobotHardware.h"
#include "Sensory\EVSensorIMU.h"

#include "Utils\EVMath\EVMFunctions.h"
#include "Utils\EVLogger.h"

#include "Arduino.h"

/* Idle Speed Setting */
#define L_PWM_IDLE																1520
#define R_PWM_IDLE																1520

/* Low Speed Setting */
#define L_PWM_FWD_LOW															1571
#define R_PWM_FWD_LOW															1469

#define L_PWM_BACK_LOW															1422
#define R_PWM_BACK_LOW															1622

/* Medium Speed Setting*/
#define L_PWM_FWD_MEDIUM														1571
#define R_PWM_FWD_MEDIUM														1469

#define L_PWM_BACK_MEDIUM														1422
#define R_PWM_BACK_MEDIUM														1622

/* High Speed Setting */
#define L_PWM_FWD_HIGH															1622
#define R_PWM_FWD_HIGH															1422

#define L_PWM_BACK_HIGH															1424//1422  // Experiment Result: 1424
#define R_PWM_BACK_HIGH															1615//1622  // Experiment Result: 1615

#define MAX_RPM_VELOCITY														160													
#define MAX_LINEAR_VELOCITY														0.418 //m/s
/*	Based on the datasheet, using the maximum linear velocity,
*	the servos can cover 30 cm distance in 41.8/30 = 1.4 seconds
*/

/* Rotation Offsets */
#define YAW_ROTATION_OFFSET														(90-70)

/* Servo Declarations */
static EVServos::FServoHandle SRV_LeftWheel	(L_DRIVESERVO_PIN);
static EVServos::FServoHandle SRV_RightWheel(R_DRIVESERVO_PIN);

static EVServos::FServoHandle SRV_LeftArm	(L_ARMSERVO_PIN);
static EVServos::FServoHandle SRV_RightArm	(R_ARMSERVO_PIN);
static EVServos::FServoHandle SRV_Lidar		(SONAR_SERVO_PIN);

static EVServos::FServoHandle* ServoList[5] =
{
	&SRV_LeftWheel,
	&SRV_RightWheel,
	&SRV_LeftArm,
	&SRV_RightArm,
	&SRV_Lidar
};

void AccumulateTurn(float deltaRads);

void EVServos::MoveForward_Impl(EServoSpeed speed)
{
	switch (speed)
	{
		case EServoSpeed::Low:
			SRV_LeftWheel.Write(L_PWM_FWD_LOW);
			SRV_RightWheel.Write(R_PWM_FWD_LOW);
		break;

		case EServoSpeed::Medium:
			SRV_LeftWheel.Write(L_PWM_FWD_MEDIUM);
			SRV_RightWheel.Write(R_PWM_FWD_MEDIUM);
		break;

		case EServoSpeed::High:
			SRV_LeftWheel.Write(L_PWM_FWD_HIGH);
			SRV_RightWheel.Write(R_PWM_FWD_HIGH);
		break;

		default:
			break;
	}
}

void EVServos::MoveBackward_Impl(EServoSpeed speed)
{
	switch (speed)
	{
		case EServoSpeed::Low:
			SRV_LeftWheel.Write(L_PWM_BACK_LOW);
			SRV_RightWheel.Write(R_PWM_BACK_LOW);
		break;

		case EServoSpeed::Medium:
			SRV_LeftWheel.Write(L_PWM_BACK_MEDIUM);
			SRV_RightWheel.Write(R_PWM_BACK_MEDIUM);
		break;

		case EServoSpeed::High:
			SRV_LeftWheel.Write(L_PWM_BACK_HIGH);
			SRV_RightWheel.Write(R_PWM_BACK_HIGH);
		break;

		default:
		break;
	}
}

void AccumulateTurn(float deltaRads)
{
	float prevRads = 0;
	float currRads = 0;
	float cumAngle = 0;

	while (FMath::Abs(cumAngle) < FMath::Abs(deltaRads))
	{
		// Rotation about the UP Vector
		currRads = Imu->GyroVector().X();

		cumAngle += FMath::Cumtrapz(prevRads, currRads, 1 / 50.0);
		prevRads = currRads;

		delay(20);
	}
}

void EVServos::TurnLeft_Impl(float DeltaDegree)
{
	float deltaRads = FMath::ToRadians(DeltaDegree);

	// Begin turning the wheels
	SRV_LeftWheel.Write(L_PWM_BACK_LOW);
	SRV_RightWheel.Write(R_PWM_FWD_LOW);

	// Lock the turn until the accumulate rotation is achieved
	AccumulateTurn(deltaRads);
	Stop_Impl();
}

void EVServos::TurnRight_Impl(float DeltaDegree)
{
	float deltaRads = FMath::ToRadians(DeltaDegree);

	// Begin turning the wheels
	SRV_LeftWheel.Write(L_PWM_FWD_LOW);
	SRV_RightWheel.Write(R_PWM_BACK_LOW);

	// Lock the turn until the accumulate rotation is achieved
	AccumulateTurn(deltaRads);
	Stop_Impl();
}

void EVServos::StepLeft_Impl(int deltaPWM)
{
	SRV_RightWheel.Write(SRV_RightWheel.Value() + -1*deltaPWM); // A decrease in PWM signifies increase in output for right wheel
}

void EVServos::StepRight_Impl(int deltaPWM)
{
	SRV_LeftWheel.Write(SRV_LeftWheel.Value() + deltaPWM);
}

void EVServos::Stop_Impl()
{
	SRV_LeftWheel.Write(L_PWM_IDLE);
	SRV_RightWheel.Write(R_PWM_IDLE);
}

void EVServos::Release_Impl()
{
	SRV_LeftWheel.Release();
	SRV_LeftArm.Release();
}

void EVServos::RaiseArms_Impl()
{
	SRV_RightArm.Write(120);
	SRV_LeftArm.Write(60);
}

void EVServos::LowerArms_Impl()
{
	SRV_RightArm.Write(15); //changed from 10 (arms bent 90 deg down) to 35
	SRV_LeftArm.Write(155); //changed from 170 (arms bent 90 deg down) to 135
}

void EVServos::ArrestArms_Impl()
{
	SRV_RightArm.Write(5); //changed from 10 (arms bent 90 deg down) to 35
	SRV_LeftArm.Write(175); //changed from 170 (arms bent 90 deg down) to 135
}

void EVServos::ResetArms_Impl()
{
	SRV_RightArm.Write(70); 
	SRV_LeftArm.Write(100); 
}

void EVServos::InitServos()
{
	SRV_LeftArm.Init();
	SRV_RightArm.Init();
	SRV_LeftWheel.Init();
	SRV_RightWheel.Init();
	SRV_Lidar.Init();

	if (!Imu)
	{
		LOG("Init Servo Detected no Imu!"); LOG("\n");
	}
}

EVServos::FServoHandle* EVServos::GetServo(uint8_t InPin)
{
	for (EVServos::FServoHandle* servo : ServoList)
	{
		if (servo->PinNumber() == InPin)
			return servo;
	}
}
EVServos::FServoHandle& EVServos::_DEBUG_GetLeftWheel()
{
	return SRV_LeftWheel;
}

EVServos::FServoHandle& EVServos::_DEBUG_GetRightWheel()
{
	return SRV_RightWheel;
}


/* Servo Handle Implementation */
EVServos::FServoHandle::FServoHandle(uint8_t PinNumber)
{
	m_pin = PinNumber;
}

void EVServos::FServoHandle::Init()
{
	m_servo.attach(m_pin);
}

void EVServos::FServoHandle::Write(int val)
{
	if (!m_servo.attached())
		m_servo.attach(m_pin);

	m_servo.write(val);
	m_val = val;
}

void EVServos::FServoHandle::Release()
{
	if (m_servo.attached())
		m_servo.detach();
}

void EVServos::FServoHandle::Reset()
{
	Write(90);
}

int EVServos::FServoHandle::Value() const
{
	return m_val;
}

uint8_t EVServos::FServoHandle::PinNumber() const
{
	return m_pin;
}