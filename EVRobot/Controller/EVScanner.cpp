#include "EVScanner.h"
#include "EVRobotHardware.h"
#include "EVPinMap.h"

#include "Sensory\EVSensorSonar.h"
#include "Sensory\EVSensorLidar.h"

#include "Controller\Private\EVServosImpl.h"

#include "Utils\EVLogger.h"

#include "Arduino.h"

static float usonicAverage = 0;
static float lidarAverage = 0;

// An offset of 90 implies that the (true origin angle value - offset) yields the zero
// This value is only meaningful for determining the relative orientation of the scanner
// to the body of the robot.
//
//	Offset = 90 --> Servo.Write(0) yields a relative angle of 0-90 = -90
//  Offset = 90 --> Servo.Write(90) yields a relative angle of 90-90 = 0
static const int servoDegOffset = 90;
static const int minAngle = 5;
static const int centerAngle = 90;
static const int maxAngle = 175;

// Give the scanner the ability to control this lidar servo
static EVServos::FServoHandle* lidarServo = nullptr;

void EVScanner::Calibrate(int samples)
{
	/* See AnimalFarm ISO-CK-6969 standard for naming conventions of cumulative variables */
	float cum_usonic = 0;
	float cum_lidar = 0;

	for (int i = 0; i < samples; i++)
	{
		cum_usonic += USonic->RequestMeasurement();
		cum_lidar += LiDAR->Distance();
	}

	usonicAverage = cum_usonic / samples;
	lidarAverage  = cum_lidar / samples;
}

float EVScanner::GetLidarAverage()
{
	return lidarAverage;
}

float EVScanner::GetSonarAverage()
{
	return usonicAverage;
}

bool EVScanner::Scan(FScanHit& outHitInfo, ComparisonFunc comparisonFunc)
{
	if (!lidarServo)
	{
		LOG("ERROR: Scan did not find any lidar servo"); LOG("\n");
	}
	unsigned long time = millis();
	outHitInfo.LidarDist = LiDAR->Distance();
	outHitInfo.SonarDist = USonic->RequestMeasurement();
	outHitInfo.RelativeAngle = lidarServo->Value() - servoDegOffset;

	outHitInfo.TimeStamp = millis() - time;

	if (comparisonFunc)
	{
		return comparisonFunc(outHitInfo.LidarDist, outHitInfo.SonarDist);
	}
	else
	{
		return false;
	}
}

bool EVScanner::Sweep(EV::TList<FScanHit>& outHitsInfo, int scanSamples, ComparisonFunc comparisonFunc)
{
	
	bool compareSuccess = false;
	int increments = (maxAngle - minAngle) / scanSamples;

	FScanHit hitInfo;

	for (int i = minAngle; i < maxAngle; i+= increments)
	{
		// Perform a scan at the incremental angle and store its result in the list
		lidarServo->Write(i);
		compareSuccess = Scan(hitInfo, comparisonFunc);

		outHitsInfo.Insert(hitInfo);

		if (compareSuccess)
		{
			return true;
		}
	}
	return false;
}

void EVScanner::SetDirection(EFacingDirection dir)
{
	if (!lidarServo)
	{
		return;
	}

	switch (dir)
	{
		case EFacingDirection::Center:
			lidarServo->Write(centerAngle);
		break;

		case EFacingDirection::Left:
			lidarServo->Write(minAngle);
		break;

		case EFacingDirection::Right:
			lidarServo->Write(maxAngle);
		break;
	}
}

void EVScanner::InitDefaults()
{
	lidarAverage = 0;
	usonicAverage = 0;

	lidarServo = EVServos::GetServo(SONAR_SERVO_PIN);

	if (!lidarServo)
	{
		LOG("ERROR Initializaing Scanner: No Lidar Servo found");
	}
	else
	{
		SetDirection(EFacingDirection::Center);
	}
}
