#ifndef EVM_FUNCTIONS_H
#define EVM_FUNCTIONS_H

#include <math.h>

#define EPSILON			(0.0001)
#define EPSILON_TINY	(0.0000001)

namespace FMath
{
	inline float Lerp(float A, float B, float percent)
	{
		return (1 - percent)*A + percent*B;
	}
	
	inline float Clamp(float min, float max, float val)
	{
		return val < min ? min : val > max ? max : val;
	}

	inline bool IsBetween(float min, float max, float val)
	{
		return val > min && val < max;
	}

	inline float Abs(float val)
	{
		return val > 0 ? val : val*-1;
	}

	inline bool NearlyEqual(float val1, float val2, float threshold = EPSILON)
	{
		return Abs(val1 - val2) <= threshold;
	}

	inline float ToRadians(float degrees)
	{
		return degrees * M_PI / 180;
	}

	inline float ToDegrees(float radians)
	{
		return radians * 180 / M_PI;
	}

	/*	Uses the trapezoid method for fast
	*	integration at the expense of accuracy.
	*/
	inline float Cumtrapz(float y1, float y2, float dt)
	{
		float result = (dt*(y1 + y2)) / 2.0;
		return result;
	}

	/*
	*/
	inline float Integrate(float y1, float y2, float dt)
	{
		// NOT IMPLEMENTED!
		return Cumtrapz(y1, y2, dt);
	}

	/*	Uses a first order Taylor-Series expansion
	 *  to compute the finite diveded difference
	*/
	inline float Differentiate(float y1, float y2, float dt)
	{
		float result = (y2 - y1) / dt;
		return result;
	}

	/*	This code is a direct copy of the implementation of fast inverse square root
	*	from the developers of Quake Arena. I take no credits for it.
	*
	*	The algorithm uses a hack followed by a 1-step Newtwon approximation for improved
	*	results. More can be found here: https://www.geeksforgeeks.org/fast-inverse-square-root/
	*/
	inline float InvSqrt(float num)
	{
		const float threehalfs = 1.5F;

		float x2 = num * 0.5F;
		float y = num;

		// evil floating point bit level hacking 
		long i = *(long *)&y;

		// value is pre-assumed 
		i = 0x5f3759df - (i >> 1);
		y = *(float *)&i;

		// 1st iteration 
		y = y * (threehalfs - (x2 * y * y));

		// 2nd iteration, this can be removed 
		// y = y * ( threehalfs - ( x2 * y * y ) ); 

		return y;
	}

	inline float Sqrt(float num)
	{
		if (num == 0)
			return 0;

		return (1 / InvSqrt(num));
	}

	/* Returns the inverse cosine in units radians */
	inline float ACosFast(float num)
	{
		float negate = float(num < 0);
		num = FMath::Abs(num);
		float ret = -0.0187293;
		ret = ret * num;
		ret = ret + 0.0742610;
		ret = ret * num;
		ret = ret - 0.2121144;
		ret = ret * num;
		ret = ret + 1.5707288;
		ret = ret * Sqrt(1.0 - num);
		ret = ret - 2 * negate * ret;
		return negate * 3.14159265358979 + ret;
	}
}
#endif