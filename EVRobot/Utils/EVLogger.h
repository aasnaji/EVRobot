#ifndef EV_LOGGER_H
#define EV_LOGGER_H

#include "Printable.h"

namespace EVUtils
{
	class Logger;
}
/*	Singleton Class that facilitates debug logging capabilities.
*	Logs are printed to Serial.
*/
class EVUtils::Logger
{
public:
	void Log(const char*) const;
	void Log(char) const;
	void Log(float) const;
	void Log(int) const;
	void Log(double) const;
	void Log(Printable&) const;

	const static Logger* Instance();
private:
	Logger();
	~Logger();

	static Logger* m_instance;
};

#ifndef _VMDEBUG
#define LOG(msg)		(EVUtils::Logger::Instance()->Log(msg))
#else
#define LOG(msg)
#endif // !_NDEBUG


#endif // !EV_LOGGER_H