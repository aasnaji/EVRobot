#include "EVLED.h"
#include "Arduino.h"

EVLighting::LED::LED(uint8_t R_pin, uint8_t G_pin, uint8_t B_pin)
{
	RGBPins[0] = R_pin;
	RGBPins[1] = G_pin;
	RGBPins[2] = B_pin;
}

void EVLighting::LED::Attach()
{
	pinMode(RGBPins[0], OUTPUT);
	pinMode(RGBPins[1], OUTPUT);
	pinMode(RGBPins[2], OUTPUT);	
}

void EVLighting::LED::SetColor(const FColor & color)
{
	Color = color;
	TurnOn();
}

EVLighting::FColor EVLighting::LED::GetColor() const
{
	return Color;
}

void EVLighting::LED::TurnOn()
{
	analogWrite(RGBPins[0], Color.R());
	analogWrite(RGBPins[1], Color.G());
	analogWrite(RGBPins[2], Color.B());
}

void EVLighting::LED::TurnOff()
{
	analogWrite(RGBPins[0], 0);
	analogWrite(RGBPins[1], 0);
	analogWrite(RGBPins[2], 0);
}
