#ifndef EV_COLOR_H
#define EV_COLOR_H
#include <stdlib.h>
namespace EVLighting
{
	struct FColor
	{
	public:
		FColor()
		{
			data = (unsigned char *)(malloc(sizeof(unsigned char) * 3));
			data[0] = 255;
			data[1] = 255;
			data[2] = 255;
		}

		FColor(unsigned char R, unsigned char G, unsigned char B)
		{
			data = (unsigned char *)(malloc(sizeof(unsigned char) * 3));
			data[0] = R;
			data[1] = G;
			data[2] = B;
		}

		~FColor()
		{
			free(data);
			data = 0;
		}

		/* RGB Accessors*/
		unsigned char R() const
		{
			return data[0];
		}

		unsigned char G() const
		{
			return data[1];
		}

		unsigned char B() const
		{
			return data[2];
		}

		/* Color Methods */
		static inline FColor Lerp(const FColor& A, const FColor& B, float alpha)
		{
			unsigned char r = (1 - alpha)*A.R() + alpha*B.R();
			unsigned char g = (1 - alpha)*A.G() + alpha*B.G();
			unsigned char b = (1 - alpha)*A.B() + alpha*B.B();

			return FColor(r, g, b);
		}

		inline void BlendAdd(const FColor& Other)
		{
			data[0] += Other.R();
			data[1] += Other.G();
			data[2] += Other.B();
		}

		inline void BlendMultiply(const FColor& Other)
		{
			data[0] *= Other.R();
			data[1] *= Other.G();
			data[2] *= Other.B();
		}

		inline void BlendDivide(const FColor& Other)
		{
			data[0] /= Other.R();
			data[1] /= Other.G();
			data[2] /= Other.B();
		}

		inline void BlendSubtract(const FColor& Other)
		{
			data[0] -= Other.R();
			data[1] -= Other.G();
			data[2] -= Other.B();
		}


		inline FColor operator +(const FColor& Other)
		{
			return FColor(data[0] + Other.R()
						, data[1] + Other.G()
						, data[2] + Other.B());
		}

		/* Common Colors */
		static FColor Red()
		{
			return FColor(255, 0, 0);
		}

		static FColor Green()
		{
			return FColor(0, 255, 0);
		}

		static FColor Blue()
		{
			return FColor(0, 0, 255);
		}

		static FColor White()
		{
			return FColor(255, 255, 255);
		}

		static FColor Yellow()
		{
			return FColor(255, 255, 0);
		}

		static FColor Orange()
		{
			return FColor(255, 165, 0);
		}

		static FColor Magenta()
		{
			return FColor(255, 0, 255);
		}

		static FColor Purple()
		{
			return FColor(128, 0, 128);
		}

		static FColor Teal()
		{
			return FColor(0, 128, 128);
		}

		static FColor Cyan()
		{
			return FColor(0, 255, 255);
		}


	private:
		unsigned char* data;
	};
}

#endif // !EV_COLOR_H