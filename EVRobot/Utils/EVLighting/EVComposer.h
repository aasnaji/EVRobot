#ifndef EV_COMPOSER_H
#define EV_COMPOSER_H

#include "EVLED.h"

namespace EVLighting
{
	class Composer;
}

/*	Light Composer
*
*	This class is responsible for interfacing with the application to provide
*	high level control and manipulation of an RGB LED circuit given the output
*	PWM digital pins to read from. The "Perform" functions are extensive and
*	are looping methods; hence, they are only called for initialization
*	and post-initialization scenarios.
*/
class EVLighting::Composer
{
public:
	Composer(uint8_t R_pin, uint8_t G_pin, uint8_t B_pin);
	~Composer();

	void Init();
	void PerformStartDisplay();
	void PerformEndDisplay();

	void ToggleSiren();
	
	void OnTargetDetect();
	void OnRampDetect();
	void OnTurn();
	
	void Signal(const FColor& color);
private:

	unsigned long m_lastBlinkTime;
	LED* m_led;
	bool m_toggleFlag;
};
#endif // !EV_LIGHT_COMPOSER

