#ifndef EV_LED_H
#define EV_LED_H

#include "EVColor.h"
#include "inttypes.h"

namespace EVLighting
{
	struct LED
	{
	public:
		LED(uint8_t R_pin, uint8_t G_pin, uint8_t B_pin);
		
		void Attach();
		
		void SetColor(const FColor& color);
		FColor GetColor() const;

		void TurnOn();
		void TurnOff();
	private:
		uint8_t RGBPins[3];
		FColor Color;
	};

}
#endif // !EV_LED_H
