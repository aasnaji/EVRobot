#include "EVComposer.h"
#include "Arduino.h"

using namespace EVLighting;

Composer::Composer(uint8_t R_pin, uint8_t G_pin, uint8_t B_pin)
	: m_led(new LED(R_pin, G_pin, B_pin))
	, m_toggleFlag(false)
{
}

Composer::~Composer()
{
}

void Composer::Init()
{
	m_led->Attach();
	m_led->TurnOff();
}

void Composer::PerformStartDisplay()
{
	// Define our Pallet
	FColor colorRed(155, 0, 0);
	FColor colorPurple = FColor::Purple();
	FColor colorGreen(45, 70, 0);

	// Alternate between the pallet colors
	m_led->SetColor(colorRed);
	delay(200);

	m_led->SetColor(colorPurple);
	delay(200);

	m_led->SetColor(colorGreen);
	delay(300);

	// Apply a gradient from red to green
	for (int i = 0; i < 100; i++)
	{
		FColor result = FColor::Lerp(colorRed, colorGreen, i / 100.0);
		m_led->SetColor(result);
		delay(40);
	}

	// Apply a gradient from green to purple
	for (int i = 0; i < 100; i++)
	{
		FColor result = FColor::Lerp(colorGreen, colorPurple, i / 100.0);
		m_led->SetColor(result);
		delay(40);
	}

	m_led->SetColor(colorRed + colorGreen);
	delay(200);

	m_led->SetColor(FColor::Cyan());
	delay(200);

	delay(200);

	m_led->TurnOff();
}

void Composer::PerformEndDisplay()
{
	// Define Our Pallet
	FColor colorCyan = FColor::Cyan();
	FColor colorYellow = FColor::Yellow();
	FColor colorBlue = FColor::Blue();
	FColor colorPurple = FColor::Purple();

	m_led->SetColor(colorCyan);
	delay(300);

	// Toggle colors fast in an orderly fashion
	for (int i = 0; i < 50; i++)
	{
		if (i % 3)
		{
			m_led->SetColor(colorYellow);
		}
		else if (i % 2)
		{
			m_led->SetColor(colorPurple);
		}
		else
		{
			m_led->SetColor(colorBlue);
		}
		delay(45);
	}


	for (int i = 0; i < 100; i++)
	{
		FColor result = FColor::Lerp(FColor::Red(), colorCyan, i / 100.0);
		m_led->SetColor(result);
		delay(40);
	}

}

void Composer::ToggleSiren()
{
	if (millis() - m_lastBlinkTime > 125)
	{
		m_led->SetColor(m_toggleFlag ? FColor::Red() : FColor::Blue());
		m_lastBlinkTime = 0;
		m_toggleFlag = !m_toggleFlag;
	}
}

void Composer::OnTargetDetect()
{
	// Set a specific color to indicate target detection
	m_led->SetColor(FColor::Green());
}

void Composer::OnRampDetect()
{
	// Set a specific color to indicate ramp detection
	m_led->SetColor(FColor::Cyan());
}

void Composer::OnTurn()
{
	if (millis()-m_lastBlinkTime > 125)
	{
		// Toggles between orange and off as an "indicator light"
		m_led->SetColor(m_toggleFlag ? FColor::Orange() : FColor(0,0,0));
		m_lastBlinkTime = 0;
		m_toggleFlag = !m_toggleFlag;
	}
}

void Composer::Signal(const FColor & color)
{
	m_led->SetColor(color);
}
