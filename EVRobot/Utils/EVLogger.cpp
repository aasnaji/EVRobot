#include "EVLogger.h"
#include "Print.h"
#include "Arduino.h"

using namespace EVUtils;

Logger* Logger::m_instance = nullptr;

void Logger::Log(const char* msg) const
{
	Serial.print(msg);
}

void Logger::Log(char msg) const
{
	Serial.print(msg);
}

void Logger::Log(float msg) const
{
	Serial.print(msg);
}

void Logger::Log(int msg) const
{
	Serial.print(msg);
}

void Logger::Log(double msg) const
{
	Serial.print(msg);
}

void Logger::Log(Printable& msg) const
{
	Serial.print(msg);
}

const Logger* Logger::Instance()
{
	if (m_instance == nullptr)
	{
		m_instance = new Logger();
	}

	return m_instance;
}

Logger::Logger()
{
	Serial.println("Logger Initialized");
	delay(5);
}

Logger::~Logger()
{
}
