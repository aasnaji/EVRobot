#ifndef EV_STATES_H
#define EV_STATES_H

#include "inttypes.h"

namespace EVStates
{
	namespace Manager
	{
		enum class EStates : uint8_t
		{
			RampDetection = 0,
			RampState,
			TargetDetection,
			Undefined,
			DEBUG_SWITCH
		};

		/* Continously called in a loop to process the robot's current state */
		void Update();

		/*	Changes the Robot current state, and hence redirection the execution
		*	flow and scope. Invokes OnStateEnd to signal the ending of the previous
		*	state before invoking OnStateBegin for the newly assigned state.
		*/
		void BeginState(EStates state);

		/*	The state manager is able to pre-process variables and objects
		*	before a desired state is engaged. This is helpful to tweak persistent
		*	variables from one transition to another.
		*/
		void OnStateBegin();

		/*	The state manager is able to post-process variables and objects
		*	after the current state is terminated. This is useful to conclude
		*	and tweak persistent objects after a state is finalized.
		*/
		void OnStateEnd();

		/*	Polls hardware and signals their events if applicable
		*/
		void ProcessEvents();
	}

}
#endif // !EV_STATES_H

