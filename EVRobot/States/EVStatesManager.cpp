#include "EVStatesManager.h"

#include "EVRampDetection.h"
#include "EVRampTraverse.h"
#include "EVTargetDetection.h"
#include "EVDEBUG_DRIVE.h"

#include "Controller\EVDrive.h"
#include "Controller\EVScanner.h"

#include "EVRobotHardware.h"
#include "Sensory\EVSensorLimitSwitch.h"
#include "Sensory\EVSensorIMU.h"
#include "Sensory\EVSensorLidar.h"

#include "Utils\EVLogger.h"

#include "Arduino.h"

using namespace EVStates;

typedef void(*StateFunctions)();

/*	Function Pointers corresponding the update loop that executes
*	based on the robot's current state. Each state loop maintains
*	its own local scope. If the EState enum is modified, the change
*	IN UNDERLYING INT MUST BE REFLECTED IN THIS ARRAY TO MATCH THE INDEXING.
*/
static StateFunctions stateFunctions[] =
{
	&RampDetection::Update,
	&RampTraverse::Update,
	&TargetDetection::Update,
	nullptr,
	&DEBUG_DRIVE::Update
};

/* The current state being processed */
static Manager::EStates currentState = Manager::EStates::Undefined;

/* Hardware Flags */
#define FLAG_LEFT		0
#define FLAG_RIGHT		1
#define FLAG_BOT		2
#define FLAG_TOP		3
static int LimSwitchesFlag = 0xC;	// 1100

#define YAW_90_DEGREES				65.8

/***********************************************************************/



void Manager::Update()
{
	/* Process Hardware/Sensor Events First */
	//ProcessEvents();
	
	if (!stateFunctions[static_cast<uint8_t>(currentState)])
	{
		LOG("STATE ERROR: State Function Invalid!"); LOG("\n");
		abort();	// Comment out of if you wish for program execution to resume
	}
	else
	{
		stateFunctions[static_cast<uint8_t>(currentState)]();
	}
	
}


void Manager::BeginState(EStates state)
{
	// Handle end of current state
	OnStateEnd();

	currentState = state;

	OnStateBegin();
}

/* Pre-process before the state begins executing*/
void Manager::OnStateBegin()
{
	switch (currentState)
	{
		case EStates::RampDetection:
			EVDrive::ReverseDriveConvention();
			EVScanner::SetDirection(EVScanner::EFacingDirection::Right);
			EVDrive::MoveForward(EServoSpeed::High);
		break;

		case EStates::RampState:
			EVDrive::ResetDriveConvention();
			EVDrive::MoveForward(EServoSpeed::High);
			EVDrive::LowerArms();
		break;

		case EStates::TargetDetection:
			
			// Drive forward for 2 seconds to ensure ramp clearance and alignment against wall
			EVDrive::MoveForward(EServoSpeed::High);
			delay(2000);

			EVDrive::MoveBackward(EServoSpeed::High);
			EVDrive::RaiseArms();
			delay(400);

			EVDrive::ReverseDriveConvention();
			EVDrive::TurnLeft(90);

			// Hold movement for 400 ms to ensure ramp clearance
			EVDrive::MoveForward(EServoSpeed::High);
			delay(400);

			// Begin Scanner calibration when stationary only
			EVDrive::Stop();
			EVScanner::Calibrate(20);
			LOG("Calibrated Scanner"); LOG("\n");
			LOG("Lidar Avg: "); LOG(EVScanner::GetLidarAverage());
			LOG("Sonar Avg: "); LOG(EVScanner::GetSonarAverage());

			EVStates::TargetDetection::InitializeState();

			// Begin moving and enter the target detection state
			EVDrive::MoveForward(EServoSpeed::High);

			// Bind Limit Switch Events to this state
		break;

		case EStates::DEBUG_SWITCH:
			EVDrive::ReverseDriveConvention();
			EVDrive::Stop();
			delay(5);
			EVDrive::MoveForward(EServoSpeed::High);
			delay(5);
		break;

		default:
			break;
	}
}

/* Post-process after state ends */
void Manager::OnStateEnd()
{
	switch (currentState)
	{
		case EStates::RampDetection:
			RampDetection::InitializeState();
		break;

		// Rotate to face the remainder of the course
		case EStates::RampState:
			RampTraverse::InitializeState();
			EVDrive::ResetArms();
			EVDrive::Stop();
			delay(10);
		break;

		case EStates::TargetDetection:
			TargetDetection::InitializeState();
		break;

		case EStates::DEBUG_SWITCH:
		break;

		default:
			break;
	}
}

void Manager::ProcessEvents()
{
	// Exclusive ORing to check a transition between limit switch states
	// This is the consequence when you dont have a Tick system ;_;
	if (LimSwitch_Bottom->IsTriggered())
	{
		LimSwitch_Bottom->OnTrigger();
	}

	if (LimSwitch_Top->IsTriggered())
	{
		LimSwitch_Top->OnTrigger();
	}


	/*
	if (LimSwitch_Left->IsTriggered() ^ (LimSwitchesFlag & 1 << FLAG_LEFT))
	{
		LimSwitchesFlag ^= 1 << FLAG_LEFT;
		LimSwitch_Left->OnTransition();
	}
	
	if (LimSwitch_Right->IsTriggered() ^ (LimSwitchesFlag & 1 << FLAG_RIGHT))
	{
		LimSwitchesFlag ^= 1 << FLAG_RIGHT;
		LimSwitch_Left->OnTransition();
	}

	
	if (LimSwitch_Bottom->IsTriggered() ^ (LimSwitchesFlag & 1 << FLAG_BOT))
	{
		LimSwitchesFlag ^= 1 << FLAG_BOT;
		LimSwitch_Left->OnTransition();
	}
	*/
}
