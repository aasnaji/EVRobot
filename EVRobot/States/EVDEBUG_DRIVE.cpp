#include "EVDEBUG_DRIVE.h"

#include "EVRobotHardware.h"
#include "Sensory\EVSensorLimitSwitch.h"

#include "Utils\EVLogger.h"

#include "Controller\Private\EVServosImpl.h"
#include "Arduino.h"

using namespace EVStates;

EVServos::FServoHandle& leftWheel = EVServos::_DEBUG_GetLeftWheel();
EVServos::FServoHandle& rightWheel = EVServos::_DEBUG_GetRightWheel();

uint16_t leftPWM;
uint16_t rightPWM;

bool rightSelected = false;

void DEBUG_DRIVE::Update()
{
	
	if (LimSwitch_Top->IsTriggered())
	{
		rightSelected = !rightSelected;
		LOG("Toggle Right Wheel: "); LOG(rightSelected); LOG("\n");
	}

	if (LimSwitch_Left->IsTriggered())
	{
		LOG("Left Switch \n");
		if (!rightSelected)
		{
			leftPWM = leftWheel.Value();
			leftPWM++;
			leftWheel.Write(leftPWM);
		}
		else
		{
			rightPWM = rightWheel.Value();
			rightPWM--;
			rightWheel.Write(rightPWM);
		}
		LOG("L: "); LOG((int)leftPWM); LOG("\t"); LOG("R: "); LOG((int)rightPWM); LOG("\n");
		delay(5);
	}

	if (LimSwitch_Right->IsTriggered())
	{
		LOG("Right Switch \n");
		if (!rightSelected)
		{
			leftPWM = leftWheel.Value();
			leftPWM--;
			leftWheel.Write(leftPWM);
		}
		else
		{
			rightPWM = rightWheel.Value();
			rightPWM++;
			rightWheel.Write(rightPWM);
		}
		LOG("L: "); LOG((int)leftPWM); LOG("\t"); LOG("R: "); LOG((int)rightPWM); LOG("\n");
		delay(5);
	}
}

void DEBUG_DRIVE::OnLimitSwitchTrigger(EVSensors::LimitSwitch* InSwitch, bool On)
{
	/*
	leftPWM = leftWheel.Value();
	rightPWM = rightWheel.Value();

	if (InSwitch == LimSwitch_Left)
	{
		leftPWM += 1;
		leftWheel.Write(leftPWM);

		LOG("Left"); LOG(leftWheel.Value()); LOG("\n");
	}
	else  
	{
		rightPWM -= 1;
		rightWheel.Write(rightPWM);

		LOG("Right"); LOG(rightWheel.Value()); LOG("\n");
	}
	LOG("LimSwitch Trigger with value: "); LOG(On); LOG("\n");
	*/
}