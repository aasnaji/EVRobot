#include "EVTargetDetection.h"
#include "EVRobotHardware.h"

#include "Controller\EVScanner.h"
#include "Controller\EVDrive.h"

#include "Sensory\EVSensorLidar.h"
#include "Sensory\EVSensorLimitSwitch.h"

#include "Utils\EVLogger.h"
#include "Utils\EVMath\EVMMath.h"

#include "Arduino.h"

using namespace EVStates;

#define YAW_90_DEGREES				65.8

/*  Store detection target information in this struct.
*	This allows us to retract our position back
*/
struct FTargetData
{
	float RequiredRotationDeg;
	float RequiredDistance;
	unsigned long ElapsedClearanceTime;
	unsigned long ElapsedArrivalTime;
	bool bIsFullyMapped;
	bool bIsVisited;
};

/* "Local" variables pertaining to the target detection state */
static FTargetData* target = nullptr;	// Initially no target found
static float wallOffsetDistance = 0;	// Distance to hug the wall by


bool IsTarget(float lidarDist, float sonarDist);
bool IsTarget_Far(float lidarDist, float sonarDist);
bool IsWall(float lidarDist, float sonarDist);
void ValidateTarget();
void PostValidateTarget();
float CalculateToRotation(const EVScanner::FScanHit& hitInfo);
float CalibrateSecondaryLidar(int samples);

void OnHitBorder();
void OnHitTarget();

void TargetDetection::Update()
{
	EVScanner::FScanHit hitInfo;

	// Preprocessor Limit Switch Events
	if (LimSwitch_Top->IsTriggered())
	{
		OnHitBorder();
	}

	if (LimSwitch_Bottom->IsTriggered())
	{
		OnHitTarget();
	}

	if (!target)
	{
		// Emit a scan and check if a target is detected.
		if (EVScanner::Scan(hitInfo), IsTarget_Far)
		{
			LOG("Scan HIT\n");
			// Can use either calculated angle or relative angle from scanner.
			// Preferably the former
			target = new FTargetData();
			target->RequiredDistance = hitInfo.LidarDist;
			target->RequiredRotationDeg = hitInfo.RelativeAngle;
			target->bIsFullyMapped = false;
			target->bIsVisited = false;
		}
	}

	// Once a target is detected, attempt to find its end
	else
	{
		// Unmapped, validate
		if (!target->bIsFullyMapped)
		{
			LOG("Validating target\n");
			// Validate the targer is indeed the post by ensuring its 2nd edge is found
			void ValidateTarget();

			if (target->bIsFullyMapped)
			{
				LOG("Target fully mapped, post-validating \n");
				PostValidateTarget();
				EVDrive::Stop();
				EVDrive::TurnLeft(YAW_90_DEGREES);
				EVDrive::MoveForward(EServoSpeed::High);
				target->ElapsedArrivalTime = millis();
			}
			else
			{
				LOG("False positive, resetting target\n");
				// The previous detection is deemed a false positive, reset search
				delete target;
				target = nullptr;
			}
		}

		// Fully Mapped
		else 
		{
			
		}
	}

	// Keep hugging the wall if applicable
	EVDrive::ApplyDeviationCorrection(30, LiDAR2->Distance());
}

void EVStates::TargetDetection::InitializeState()
{
	EVDrive::Stop();
	delay(10);
	CalibrateSecondaryLidar(10);
	EVDrive::MoveForward(EServoSpeed::High);
	
}

bool IsTarget(float lidarDist, float sonarDist)
{
	return ((lidarDist < EVScanner::GetLidarAverage() - 10) && (EVScanner::GetSonarAverage() < EVScanner::GetSonarAverage() - 10));
}

bool IsTarget_Far(float lidarDist, float sonarDist)
{
	return ((lidarDist < EVScanner::GetLidarAverage() - 10) || (EVScanner::GetSonarAverage() < EVScanner::GetSonarAverage() - 5));
}

bool IsWall(float lidarDist, float sonarDist)
{
	return (/*(distance_tof > (tof_average-5) )  ||*/ (sonarDist > (EVScanner::GetSonarAverage() - 5)));
}

void ValidateTarget()
{
	EVDrive::Stop();
	
	unsigned long timeoutTimer = 0;
	delay(700);

	EVDrive::MoveForward(EServoSpeed::High);
	EVScanner::FScanHit hit;

	// Validate the target by finding its other edge. The edge is deemed valid
	// so long as the timeout has not been exceed and the elapsed time is greater
	// than a marginal noisy time window.
	while (!target->bIsFullyMapped && timeoutTimer < 4000)
	{
		target->bIsFullyMapped = EVScanner::Scan(hit, IsWall) && timeoutTimer > 300;
		timeoutTimer += (millis() - timeoutTimer);
	}

	target->ElapsedClearanceTime = timeoutTimer;
}

void PostValidateTarget()
{
	EVDrive::Stop();
	delay(300);

	EVDrive::MoveBackward(EServoSpeed::High);
	delay(target->ElapsedClearanceTime / 2.0);

	EVScanner::SetDirection(EVScanner::EFacingDirection::Center);
	EVDrive::TurnLeft(YAW_90_DEGREES);
	EVDrive::MoveForward(EServoSpeed::High);

}

float CalculateToRotation(const EVScanner::FScanHit& hitInfo)
{
	return 90;
}

float CalibrateSecondaryLidar(int samples)
{
	float cum_lidar = 0;

	for (int i = 0; i < samples; i++) 
	{
		cum_lidar += LiDAR2->Distance();
	}

	wallOffsetDistance = cum_lidar/samples;
}

void OnHitBorder()
{
	EVDrive::Stop();
}

void OnHitTarget()
{
	// Assume we reached our target!
	target->ElapsedArrivalTime = millis() - target->ElapsedArrivalTime;
	EVDrive::Stop();
	delay(100);

	EVDrive::MoveBackward(EServoSpeed::High);
	delay(target->ElapsedArrivalTime);
}
