#ifndef EVDEBUG_DRIVE_H
#define EVDEBUG_DRIVE_H

namespace EVSensors
{
	class LimitSwitch;
}

namespace EVStates
{
	namespace DEBUG_DRIVE
	{
		void Update();

		void OnLimitSwitchTrigger(EVSensors::LimitSwitch* InSwitch, bool On);
	}
}
#endif // !EVDEBUG_DRIVE_H
