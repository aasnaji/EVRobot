#include "EVRampTraverse.h"
#include "EVStatesManager.h"

#include "EVRobotHardware.h"
#include "Sensory\EVSensorIMU.h"

#include "Controller\EVDrive.h"

#include "Utils\EVMath\EVMMath.h"
#include "Utils\EVLogger.h"
#include "Arduino.h"

using namespace EVStates;

/* "Local" variables pertaining to the ramp traversal state */
#define	RAMP_UP_ACCEL					4.8
#define RAMP_FLAT_ACCEL					9.0
#define RAMP_DOWN_ACCEL					-3.0

#define DESCEND_DURATION				3500 //1750

// Represent slope changes; helps prevents false positive
// because we can check for invalid transitions
enum class EIMUSlope : uint8_t
{
	Incline = 0x1,
	Flat = 0x2,
	Decline = 0x4
};

static EIMUSlope CurrentSlope;
bool IsGuided = false;
bool IsRampCleared = false;

void ProcessSlope();
void BeginSlide();

void RampTraverse::Update()
{
	ProcessSlope();

	switch (CurrentSlope)
	{
		case EIMUSlope::Incline:
			if (!IsGuided)
			{
				EVDrive::ArrestArms();
				IsGuided = true;
			}
		break;

		case EIMUSlope::Decline:
			BeginSlide();
		break;

		default:
			break;
	}

	if (IsRampCleared)
	{
		Manager::BeginState(Manager::EStates::TargetDetection);
	}
}

void ProcessSlope()
{
	FVector accel = Imu->AccelVector();

	// Ramp Up
	if (accel.X() < RAMP_FLAT_ACCEL && accel.Z() > RAMP_UP_ACCEL)
	{
		CurrentSlope	= CurrentSlope == EIMUSlope::Flat
						? EIMUSlope::Incline
						: CurrentSlope;
	}

	// Ramp Down
	else if (accel.X() < RAMP_FLAT_ACCEL && accel.Z() < RAMP_DOWN_ACCEL)
	{
		CurrentSlope	= CurrentSlope == EIMUSlope::Flat
						? EIMUSlope::Decline
						: CurrentSlope;
	}

	// Flat
	else
	{
		CurrentSlope = CurrentSlope != EIMUSlope::Flat
					? EIMUSlope::Flat
					: CurrentSlope;
	}

#if 0	/* [DEPRECATED] */
	// Pitch occurs due to UP axis of accelertion (Z)
	// The FORWARD axis tells us direction of incline/decline (Y)
	
	// Ramp Up
	if (accel.Z() < RAMP_FLAT_ACCEL && accel.Y() > RAMP_UP_ACCEL)
	{
		bool isValid = IMUSlopeFlag & (1 << FLAG_IMU_FLAT) && !(IMUSlopeFlag & (1 << FLAG_IMU_DECLINE));

		return isValid ? 1 << FLAG_IMU_INCLINE : IMUSlopeFlag;
	}

	// Ramp Down
	else if (accel.Z() < RAMP_FLAT_ACCEL && accel.Y() < RAMP_DOWN_ACCEL)
	{
		// This reading is invalid we are NOT flat and are somehow inclined.
		bool isValid = IMUSlopeFlag & (1 << FLAG_IMU_FLAT) && !(IMUSlopeFlag & (1 << FLAG_IMU_INCLINE));

		// Indicates Ramping Down, Return the new flag with decline only switched on.
		// Otherwise return the unmodified flag
		return isValid ? 1 << FLAG_IMU_DECLINE : IMUSlopeFlag;
	}

	// Flat
	else
	{
		// This reading is invalid if we are incline and declined at the same time.
		bool isValid = !((IMUSlopeFlag & 1 << FLAG_IMU_DECLINE) && (IMUSlopeFlag & 1 << FLAG_IMU_INCLINE));

		return isValid ? 1 << FLAG_IMU_FLAT : IMUSlopeFlag;
	}
#endif
}

void BeginSlide()
{
	EVDrive::Release();

	unsigned long currentTime = 0;
	unsigned long timeElapsed = 0;
	unsigned long startTime = millis();

	while (timeElapsed < DESCEND_DURATION) 
	{
		currentTime = millis();
		timeElapsed = currentTime - startTime;
	}
	
	EVDrive::RaiseArms();
	EVDrive::Stop();

	IsRampCleared = true;
}

void RampTraverse::InitializeState()
{
	IsGuided = false;
	IsRampCleared = false;
	ProcessSlope();
}
