#ifndef EV_RAMP_TRAVERSE_H
#define EV_RAMP_TRAVERSE_H

#include "inttypes.h"

namespace EVStates
{
	namespace RampTraverse
	{
		void Update();

		void InitializeState();
	}
}
#endif // !EV_RAMP_TRAVERSE_H
