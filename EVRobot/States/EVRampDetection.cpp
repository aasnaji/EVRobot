#include "EVRampDetection.h"
#include "EVStatesManager.h"

#include "Controller\EVDrive.h"

#include "EVRobotHardware.h"
#include "Sensory\EVSensorLimitSwitch.h"
#include "Sensory\EVSensorLidar.h"
#include "Sensory\EVSensorIMU.h"

#include "Utils\EVLogger.h"

#include "Utils\EVMath\EVMFunctions.h"

#include "Arduino.h"

using namespace EVStates;

/*	"Local" Variables pertaining to the ramp detection stage
*/
#define RAMP_LOWER_THRESHOLD		20
#define RAMP_UPPER_THRESHOLD		75
#define RAMP_OOB_THRESHOLD			90

#define FLAG_RAMP_EDGE1				1	// DEPRECATED
#define FLAG_RAMP_EDGE2				2	// DEPRECATED
#define	FLAG_RAMP_MIDDLE			3	// DEPRECATED
#define FLAG_RAMP_ALIGNED			4	// DEPRECATED

#define YAW_90_DEGREES				65.8

// At the beginning, all flags are false
static uint8_t RampDetectionFlag = 0;	// DEPRECATED
bool IsEdge1Detected = false;
bool IsEdge2Detected = false;
bool IsRampMiddleDetected = false;
bool IsRampAligned = false;

static unsigned long FirstEdgeTime = 0;
static unsigned long SecondEdgeTime = 0;
static unsigned long BackTrackTime = 0;

/* Private Function Prototypes*/
void FindFirstEdge();
void FindSecondEdge();
void FindRampMiddle();
void AlignToRamp();

/*	This is where the actual state machine process occurs
*	
*	Trigger different handlers based on the state's current progress
*/
void RampDetection::Update()
{
#if 0	/* [DEPRECATED]*/
	if ( !(RampDetectionFlag & 1 << FLAG_RAMP_EDGE1) )
		FindFirstEdge();

	if ( !(RampDetectionFlag & 1 << FLAG_RAMP_EDGE2) )
		FindSecondEdge();

	if (!(RampDetectionFlag & 1 << FLAG_RAMP_MIDDLE))
		FindRampMiddle();

	if ( !(RampDetectionFlag & 1 << FLAG_RAMP_ALIGNED) )
		AlignToRamp();
#endif
	if (!IsEdge1Detected)
		FindFirstEdge();

	else if (!IsEdge2Detected)
		FindSecondEdge();

	else if (!IsRampMiddleDetected)
		FindRampMiddle();

	else if (!IsRampAligned)
		AlignToRamp();
	
	else
	// Begin the next sequential state when all stages are completed
	Manager::BeginState(Manager::EStates::RampState);
}

void FindFirstEdge()
{
	if (FMath::IsBetween(RAMP_LOWER_THRESHOLD, RAMP_UPPER_THRESHOLD, LiDAR->Distance()))
	{
		LOG("Found First Edge \n");
		EVDrive::Stop();
		IsEdge1Detected = true;

		// What is this for?
		delay(1000);
		EVDrive::MoveForward(EServoSpeed::High);

		// Record our time stamp
		FirstEdgeTime = millis();
	}
}

void FindSecondEdge()
{
	// Detected end of edge if distance turns large
	if (LiDAR->Distance() > RAMP_OOB_THRESHOLD)
	{
		LOG("Found Second Edge \n");

		SecondEdgeTime = millis();

		// Add a margin of time to ensure we truly are out of bound
		if (SecondEdgeTime - FirstEdgeTime > 75)
		{
			// Stop and notify of edge detection
			IsEdge2Detected = true;
			EVDrive::Stop();
			delay(100);

			// Begin the next sequence by moving backward to find the middle of the ramp
			EVDrive::MoveBackward(EServoSpeed::High);
			BackTrackTime = millis();
		}
	}
}

void FindRampMiddle()
{
	auto elapsedTime = millis() - BackTrackTime;

	// Middle is assumed to be reached if we travel half the distance
	// that carried us across the width of the ramp
	if (elapsedTime >= (SecondEdgeTime - FirstEdgeTime) / 2.0)
	{
		IsRampMiddleDetected = true;
		EVDrive::Stop();
		delay(1000); // What are these delays for?
	}
}

void AlignToRamp()
{
	// Turning while suspend our program in a while loop!
	EVDrive::TurnLeft(YAW_90_DEGREES);
	delay(500);
	IsRampAligned = true;
}

void RampDetection::InitializeState()
{
	// Reset Values to Factory Defaults
	RampDetectionFlag = 0;

	FirstEdgeTime = 0;
	SecondEdgeTime = 0;
	BackTrackTime = 0;

	IsEdge1Detected = false;
	IsEdge2Detected = false;
	IsRampMiddleDetected = false;
	IsRampAligned = false;
}

void RampDetection::OnLimitSwitchTriggered(EVSensors::LimitSwitch* InSwitch, bool Pressed)
{
	LOG("LimSwitch Trigger with value: "); LOG(Pressed); LOG("\n");
}

