#ifndef EV_RAMP_DETECTION_H
#define EV_RAMP_DETECTION_H

namespace EVSensors
{
	class LimitSwitch;
}

namespace EVStates
{
	namespace RampDetection
	{
		/* The primary processing method for the state
		*
		*	Track the progress of the current traversal and
		*	execute the different flow paths based on current objective
		*/
		void Update();

		void InitializeState();
		void OnLimitSwitchTriggered(EVSensors::LimitSwitch* InSwitch, bool Pressed);
	}
}
#endif // !EV_RAMP_DETECTION_H
