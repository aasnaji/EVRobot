#ifndef EV_TARGET_DETECTION_H
#define EV_TARGET_DETECTION_H

namespace EVStates
{
	namespace TargetDetection
	{
		void Update();
		void InitializeState();
	
	}
}
#endif // !EV_TARGET_DETECTION_H

