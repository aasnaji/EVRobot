#include "EVRobotHardware.h"
#include "EVPinMap.h"

#include "Sensory\EVSensorIMU.h"
#include "Sensory\EVSensorLidar.h"
#include "Sensory\EVSensorLimitSwitch.h"
#include "Sensory\EVSensorSonar.h"

#include "Wire.h"

#ifndef DUAL_LIDARS
#define LIDAR_PRIMARY_ADDRESS 0x29
#endif // DUAL_LIDARS


	// Initialize the non-I2C Global Hardware Components
	EVSensors::IMU* Imu = new EVSensors::IMU();
	EVSensors::Lidar* LiDAR = new EVSensors::Lidar(LIDAR_PRIMARY_ADDRESS, LIDAR_PRIMARY_SHT_PIN);
	EVSensors::Lidar* LiDAR2 = new EVSensors::Lidar(LIDAR_SECONDARY_ADDRESS, LIDAR_SECONDARY_SHT_PIN);
	EVSensors::Sonar* USonic = new EVSensors::Sonar(SONAR_TRIGGER_PIN, SONAR_ECHO_PIN);

	EVSensors::LimitSwitch* LimSwitch_Left = new EVSensors::LimitSwitch(L_LIMIT_SWITCH_0_PIN, true);
	EVSensors::LimitSwitch* LimSwitch_Right = new EVSensors::LimitSwitch(R_LIMIT_SWITCH_0_PIN, true);
	EVSensors::LimitSwitch* LimSwitch_Bottom = new EVSensors::LimitSwitch(BOT_LIMIT_SWITCH_0_PIN, true);
	EVSensors::LimitSwitch* LimSwitch_Top = new EVSensors::LimitSwitch(TOP_LIMIT_SWITCH_0_PIN, 0);
