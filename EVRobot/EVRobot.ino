/*
 Name:		EVRobot.ino
 Created:	11/18/2018 11:49:51 AM
 Author:	Naji
*/

#include "Controller\EVDrive.h"
#include "Controller\EVScanner.h"

#include "States\EVStatesManager.h"
#include "EVRobotHardware.h"
#include "EVPinMap.h"

#include "Utils\EVLogger.h"

#include "Sensory\EVSensorLidar.h"
#include "Sensory\EVSensorIMU.h"

#include "Wire.h"

#include "Arduino.h"

// the setup function runs once when you press reset or power the board
void setup() 
{
	delay(50);
	Serial.begin(9600);

	// Initialize I2C bus settings
	Wire.begin();
	Wire.setClock(I2C_CLK_SPEED);
	delay(50);

	/* Initializes Robot States and Controllers */
	InitHardware();

	delay(25);

	LOG("Setup EVRobot"); LOG("\n");

	EVDrive::Stop();
	EVStates::Manager::BeginState(EVStates::Manager::EStates::TargetDetection);
}

// the loop function runs over and over again until power down or reset
void loop() 
{
	EVStates::Manager::Update();
	delay(75);
}

void InitHardware()
{
	/* Becaused hardware is obnoxious, can't initialize these anywhere else. */
	if (!Imu || !LiDAR)
	{
		LOG("Setup Failure: No IMU or Lidar Detected!"); LOG("\n");
	}
	Imu->Init();
	EVSensors::FLidarInitializer Initializer;

	#ifdef DUAL_LIDARS
		Initializer.InitDualLidars(LiDAR, LiDAR2);
		LiDAR2->SetClampedDistance(2000);
	#else
		Initializer.InitSingleLidar(LiDAR);
	#endif // DUAL_LIDARS
	LiDAR->SetClampedDistance(2000);
	delay(500);
	
	EVDrive::InitDrive();
	EVScanner::InitDefaults();
}